# -*- coding: utf-8 -*-

#
#   SMTP ACCOUNT FORM
#

from django import forms
from django.utils.translation import ugettext as _

from md.models.smtp_account import SmtpAccountModel
from md.regexp import get_dojo_validator, get_form_validator


class SmtpAccountForm(forms.ModelForm):
    account_name = forms.CharField(
        label=_("Account Name"),
        required=True,
        max_length=128,
        widget=forms.TextInput(attrs={
            'trim': 'true',
            'data-dojo-type': 'md/iValidationTextBox',
            'data-dojo-props': "required: true, pattern: '%s'" % get_dojo_validator('name'),
            #'validator': 'list_name_validator',
            'invalidMessage': _("Invalid account name."),
            'promptMessage': _("Account name should be unique and representative.")
        }),
        validators=[get_form_validator('name')]
    )

    host = forms.CharField(
        label=_("Host"),
        required=True,
        max_length=128,
        widget=forms.TextInput(attrs={
            'trim': 'true',
            'data-dojo-type': 'md/iValidationTextBox',
            #'data-dojo-props': "required: true, pattern: '%s'" % dojo_regex_name(),
            #'validator': 'list_name_validator',
            'invalidMessage': _("Invalid host name."),
            'promptMessage': _("Host name or IP.")
        }),
    )

    port = forms.CharField(
        label=_("Port"),
        required=True,
        max_length=5,
        widget=forms.TextInput(attrs={
            'trim': 'true',
            'data-dojo-type': 'md/iValidationTextBox',
            #'data-dojo-props': "required: true, pattern: '%s'" % dojo_regex_name(),
            #'validator': 'list_name_validator',
            'invalidMessage': _("Invalid port."),
            'promptMessage': _("Used port for SMTP. It could be 25, 465, 587.")
        }),
    )

    username = forms.CharField(
        label=_("Username"),
        required=False,
        max_length=128,
        widget=forms.TextInput(attrs={
            'trim': 'true',
            'data-dojo-type': 'md/iValidationTextBox',
            #'data-dojo-props': "required: true, pattern: '%s'" % dojo_regex_name(),
            #'validator': 'list_name_validator',
            'invalidMessage': _("Invalid username"),
            'promptMessage': _("Username used for authentication.")
        }),
    )

    '''password = forms.CharField(
        label=_("Password"),
        required=False,
        max_length=128,
        widget=forms.TextInput(attrs={
            'trim': 'true',
            'data-dojo-type': 'md/iValidationTextBox',
            #'data-dojo-props': "required: true, pattern: '%s'" % dojo_regex_name(),
            #'validator': 'list_name_validator',
            'invalidMessage': '',
            'promptMessage': _("Password used for authentication.")
        }),
    )'''

    class Meta:
        model = SmtpAccountModel
        exclude = ['created']
