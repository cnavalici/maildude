# -*- coding: utf-8 -*-
from md.forms.smtp_account import SmtpAccountForm


class FormsFactory:
    forms_map = {
        'account_smtp_addedit': {
            'form': SmtpAccountForm,
            'template': 'account_smtp_addedit.html'
        }
    }

    @staticmethod
    def factory(form_name):
        config = FormsFactory.forms_map[form_name]

        template_data = {
            'form': config['form']()
        }

        template = 'md/forms/' + config['template']

        return template, template_data
