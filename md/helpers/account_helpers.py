# -*- coding: utf-8 -*-

import StringIO
import qrcode

from pyotp import random_base32, TOTP


def generate_qrcode(otp_key=None, email_address=''):
    '''generates the QR code for OTP Enrollment as base64 encoded image'''
    if not email_address:
        raise Exception("No email address specified.")

    if otp_key is None:
        otp_key = random_base32()

    totp = TOTP(otp_key)
    link = totp.provisioning_uri(email_address)

    qr = qrcode.make(link)

    output = StringIO.StringIO()
    qr.save(output)

    encoded_img = output.getvalue().encode('base64').replace('\n', '')

    return (encoded_img, otp_key)
