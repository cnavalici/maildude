/* tab-containers/parts/account_smtp_settings.js */

define(
[
    "dojo/request", "js/config", "dojo/dom", "dojo/parser"
],
function(
    request, mdconfig, dom, parser
)
{
    return {
        cached_content: {},

        smtp_form_div: 'account_smtp_addedit',

        init: function() {
            console.log(this);
            this.load_smtp_form();
            this.load_smtp_grid();
            console.log("ainit");
        },

        load_smtp_form: function() {
            var _this = this;

            if (!_this.cached_content[_this.smtp_form_div]) {
                request
                    .get(mdconfig.urls['md-get-form'], {
                        query: {'formname': 'account_smtp_addedit'}
                    })
                    .then(function(response) {
                        var container = dom.byId(_this.smtp_form_div);
                        container.innerHTML = response;
                        parser.parse(container);

                        _this.cached_content[_this.smtp_form_div] = container;
                    });
            }
        },

        load_smtp_grid: function() {
            console.log("load grid");
        },
    }
});
