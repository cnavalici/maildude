/* tab-containers/parts/account_otp_enrollment.js */

define(
[
    "dijit/registry", "js/config", "dojo/dom-attr", "md/js/dialogs", "dijit/popup", "dojo/cookie",
    "dojo/i18n!i18n/nls/account", "dijit/TooltipDialog", "dojo/request"
],
function(
    registry, mdconfig, domAttr, dialogs, popup, cookie,
    t, TooltipDialog, request
)
{
    return {
        init: function() {
            var qr_code_id = "img_generate_otp";
            var default_qrcode_img = domAttr.get(qr_code_id, "src");

            var btn_generate_otp = registry.byId("btn_generate_otp");
            btn_generate_otp.on("click", function(evt) {
                request.get(mdconfig.urls['md-get-qr-code'], {}).then(function(response) {
                    domAttr.set(qr_code_id, "src", "data:image/png;base64," + response);
                });
            });


            var otp_test_tooltip = new TooltipDialog({
                id: 'otp_test_tooltip',
                style: "width: 300px;",
                content: "",
                onMouseLeave: function() { popup.close(this); }
            });


            var check_qr_is_generated = function() {
                // check if the current QR image is different than the default one
                if (domAttr.get(qr_code_id, "src") == default_qrcode_img) {
                   dialogs.enrol_error.show();
                   return false;
                }
                return true;
            }

            var btn_verify_otp = registry.byId("btn_verify_otp");
            btn_verify_otp.on("click", function(evt) {
                if (check_qr_is_generated()) {
                    request
                        .get(mdconfig.urls['md-get-totp-current-value'], {})
                        .then(function(response) {
                            otp_test_tooltip.set('content', t.your_current_token_should_be + response);

                            popup.open({
                                popup: otp_test_tooltip,
                                around: dom.byId(qr_code_id),
                                orient: ["after-centered", "below-centered"],
                                onCancel: function(){
                                    popup.close(otp_test_tooltip);
                                },
                            });
                        });
                }
            });


            var btn_save_otp = registry.byId("btn_save_otp");
            btn_save_otp.on("click", function(evt) {
                if (check_qr_is_generated()) {
                    request.post(mdconfig.urls['md-set-otp-key'], {
                        sync: false,
                        data: {},
                        headers: {
                            "X-CSRFToken": cookie('csrftoken')
                        }
                    }).then(function(updated) {
                        if (updated) {
                            domAttr.set(qr_code_id, "src", default_qrcode_img);
                            dialogs.enrol_save_success.show();
                        } else {
                            dialogs.enrol_save_err.show();
                        }
                    });
                }
            });

            var btn_cancel_otp_check = registry.byId("btn_cancel_otp_check");
            btn_cancel_otp_check.on("click", function(evt) {
                dialogs.enrol_cancellation_confirm.show();
            });
        }
    }
});