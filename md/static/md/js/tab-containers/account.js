/* tab-containers/account.js */

define(
[
    "js/config", "dojo/request", "dojo/dom", "dojo/parser", "dojo/Deferred",
    "md/js/tab-containers/parts/account_otp_enrollment", "md/js/tab-containers/parts/account_smtp_settings",
    //"dojo/ready", "dojo/domReady!"
],
function(mdconfig, request, dom, parser, Deferred,
         otpEnroll, smtpSettings)
{
    return {
        cached_content: {},

        tabs_metadata: {
            'tab_dashboard': {
                partname: 'account_dashboard',
                callback_object: null
            },
            'tab_otp_enrollment': {
                partname: 'account_otp_enrollment',
                callback_object: otpEnroll
            },
            'tab_export_settings': {
                partname: 'account_export_settings',
                callback_object: null
            },
            'tab_smtp_settings': {
                partname: 'account_smtp_settings',
                callback_onload: smtpSettings
            },
        },

        load_tab: function(tab_id) {
            var _this = this;

            var deferred = new Deferred();

            deferred.then(function() {
                if (!_this.cached_content[tab_id]) {
                    request
                        .get(mdconfig.urls['md-get-part'], {
                            query: {'partname': _this.tabs_metadata[tab_id].partname}
                        })
                        .then(function(response) {
                            var container = dom.byId(tab_id);
                            container.innerHTML = response;
                            parser.parse(container);

                            _this.cached_content[tab_id] = container;
                        })
                }
            }).then(function(v) {
                // callback is any
                if (_this.tabs_metadata[tab_id].callback_onload) {
                    _this.tabs_metadata[tab_id].callback_onload.init();
                }
            });

            deferred.resolve();
        }
    }
});