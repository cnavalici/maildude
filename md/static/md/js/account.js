require(
[
    "dijit/registry", "dojo/ready", "md/js/tab-containers/account",
    "dojo/domReady!"
],
function(
    registry, ready, tabAccount
)
{
    ready(function() {
        var account_tab_container = registry.byId("account_tab_container");
        account_tab_container.watch("selectedChildWidget", function(name, oval, nval){
            tabAccount.load_tab(nval.id);
        });
    });
});