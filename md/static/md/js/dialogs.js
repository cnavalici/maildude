define(
[
    "dijit/Dialog", "dojo/i18n!i18n/nls/dialogs", "dojo/request", "js/config", "dojo/cookie", "js/md-confirm-dialog",
    "dojo/domReady!"
],
function(Dialog, t, request, mdconfig, cookie, mdConfirmDialog) {
    return {
        enrol_error: new Dialog({
            title: t.missing_data,
            content: t.missing_data_msg,
            style: "width: 350px;"
        }),

        enrol_save_success: new Dialog({
            title: t.successful_op,
            content: t.successful_otp_msg,
            style: "width: 350px;"
        }),

        enrol_save_error: new Dialog({
            title: t.failed_operation,
            content: t.failed_otp_save_msg,
            style: "width: 350px;"
        }),

        enrol_cancellation_confirm: new mdConfirmDialog({
            title: t.enrol_cancellation,
            content: t.enrol_cancellation_msg,
            id: 'otp_dlg_cancellation',
            style: "width: 420px; background-color: #fff",
            onExecute: function() {
                request.post(mdconfig.urls['md-cancel-otp-check'], {
                    sync: false,
                    data: {},
                    headers: { "X-CSRFToken": cookie('csrftoken') }
                })
            }
        }),

        enrol_cancellation: new Dialog({
            title: t.enrol_cancellation,
            content: t.enrol_cancellation_msg,
            style: "width: 350px;"
        }),
    }
});