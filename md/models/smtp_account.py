# -*- coding: utf-8 -*-

from django.db import models


class SmtpAccountModel(models.Model):
    account_name = models.CharField(max_length=128, db_index=True, unique=True)
    host = models.CharField(max_length=128)
    port = models.CharField(max_length=5, default='25')
    username = models.CharField(max_length=128)
    password = models.DateTimeField(auto_now_add = True, db_index = True)

    def __unicode__(self):
        return u"{0} {1}:{2} ({3)".format(self.account_name, self.host, self.port, self.username)

    def export(self):
        return [()]

    class Meta:
        # ordering = ["-created"]
        app_label = 'md'
