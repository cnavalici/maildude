# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.http import HttpResponseRedirect

urlpatterns = patterns('md.views',
    url(r'^$', 'dashboard.index', name='dashboard'),

    # CAMPAIGNS ##############################################
    url(r'^campaigns/$', 'campaigns.index', name='campaigns_page'),

    # TEMPLATES ##############################################
    url(r'^templates/$', 'templates.index', name='templates_page'),

    # LISTS ##############################################
    url(r'^lists/$', 'lists.index', name='lists_page'),

    # MY ACCOUNT #########################################
    url(r'^get-part/$', 'account.get_part', name='account_get_part'),
    url(r'^get-form/$', 'account.get_form', name='account_get_form'),
    url(r'^my-account/$', 'account.index', name='account_page'),
    url(r'^get-qr-code/$', 'account.get_QR_code', name='get_qr_code'),
    url(r'^get-totp-current-value/$', 'account.get_TOTP_current_value', name='get_totp_current_value'),
    url(r'^set-otp-key/$', 'account.set_otp_key', name='set_otp_key'),
    url(r'^cancel-otp-check/$', 'account.cancel_otp_check', name='cancel_otp_check'),

    url(r'^$', lambda x: HttpResponseRedirect('/')),
)
