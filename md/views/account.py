# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from pyotp import TOTP

from decorators import request_is_ajax
from md.helpers.account_helpers import generate_qrcode
from md.helpers.forms_factory import FormsFactory

from auth.models import AuthUser


def index(request):
    template_data = {}

    return render_to_response(
        'md/account.html', template_data,
        context_instance=RequestContext(request)
    )


def get_part(request):
    part_name = request.GET.get('partname', '')

    return render_to_response(
        'md/parts/' + part_name + '.html',
        {},
        context_instance=RequestContext(request)
    )


def get_form(request):
    form_name = request.GET.get('formname', '')

    template, template_data = FormsFactory.factory(form_name)

    return render_to_response(
        template,
        template_data,
        context_instance=RequestContext(request)
    )





@request_is_ajax
def get_QR_code(request):
    otp_image, otp_key = generate_qrcode(email_address=request.user.email)
    request.session['generated_otp_key'] = otp_key

    return HttpResponse(otp_image)


@request_is_ajax
def get_TOTP_current_value(request):
    otp_key = request.session.get('generated_otp_key', False)

    if otp_key:
        totp = TOTP(otp_key)
        resp = "%06d" % totp.now()
    else:
        resp = 0

    return HttpResponse(resp)


@request_is_ajax
def set_otp_key(request):
    otp_key = request.session.get('generated_otp_key', False)

    if otp_key:
        resp = AuthUser.objects.filter(pk=request.user.id).update(otp_token=otp_key)
        if resp:
            request.session['generated_otp_key'] = False
    else:
        resp = 0

    return HttpResponse(resp)


@request_is_ajax
def cancel_otp_check(request):
    AuthUser.objects.filter(pk=request.user.id).update(otp_token=None)

    return HttpResponse()
