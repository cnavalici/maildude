# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

#from md.forms.my_account import AuthUserForm

def index(request):
    template_data = {}

    #template_data['frm'] = AuthUserForm()
    return render_to_response('md/dashboard.html',
                              template_data,
                              context_instance=RequestContext(request))
