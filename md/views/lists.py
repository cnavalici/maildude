# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

def index(request):
    return render_to_response('md/lists.html', {},
                              context_instance=RequestContext(request))
