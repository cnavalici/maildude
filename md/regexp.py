# -*- coding: utf-8 -*-

# REGEX USED OVERALL FOR VALIDATION
#
#  note that we're using regex library instead of re for utf8 support
#
# this helper will generate regex expression for form validators and also for dojo forms
#
# URL: http://www.regular-expressions.info/unicode.html

import regex

from django.core.validators import RegexValidator

REGEX_MAP = {
    # unicode letter _and_ digits _and_ spaces, minimum 3
    'name': "^[\p{Letter}0-9\s]{3,}$"
}


def get_form_validator(validator_name):
    regexpr = REGEX_MAP[validator_name]

    return RegexValidator(
        regex=regex.compile(regexpr, flags=regex.UNICODE),
        message=''
    )


def get_dojo_validator(validator_name):
    regexpr = REGEX_MAP[validator_name]

    return dojo_compatible_regex(regexpr)


def dojo_compatible_regex(py_regex):
    '''prepare the regex to be compatible with the dojo forms created server side'''
    return py_regex.replace('\\', '\\\\\\')
