define({
    root: {
        missing_data: "Missing data",
        missing_data_msg: "Generate first an QR code <br><br>",
        successful_op: "Successful operation",
        successful_otp_msg: "The OTP token was successfully saved. You can use it now at the next login.",
        failed_operation: "Failed operation",
        failed_otp_save_msg: "The OTP saving has failed. Please try first to refresh the page.",
        otp_cancel: "OTP Cancellation",
        otp_cancel_msg: "Your OTP was cancelled. Now you can login using only username and password",
        enrol_cancellation: "OTP Cancellation",
        enrol_cancellation_msg: "Are your sure you want to cancel OTP check?",
    },
    'ro': true
});