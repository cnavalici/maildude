define(
[
    'dojo/_base/declare', 'dijit/Dialog', 'dijit/_WidgetsInTemplateMixin', 'dijit/form/Button'
],
function(declare, Dialog, _WidgetsInTemplateMixin, template) {
	return declare('mdConfirmDialog', [Dialog, _WidgetsInTemplateMixin], {
 		title: 'Confirm Dialog',
		templateString: dojo.cache("js", "md-confirm-dialog.html"),

		constructor: function(options){
			if (options.message) {
				this.content = options.message;
			}
		}

	});
});
