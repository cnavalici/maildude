/*
 * this file contains bits and pieces with general range of usage
 */


/* custom date formatter for dGrid date columns */
datetime_format = function(item) {
    var dt = new Date(item);
    return dt.toLocaleDateString() + ' ' + dt.toLocaleTimeString();
};


/* missing sleep function for js */
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

/* improved ValidationBox with support for utf8 regex (XRegExp) */
define("md/iValidationTextBox", [ "dojo/_base/declare", "dijit/form/ValidationTextBox" ],
    function(declare, ValidationTextBox) {
        return declare([ValidationTextBox], {
            validator: function(value, constraints) {
                var computed_constraints = this._computeRegexp(constraints);
                return (new XRegExp("^(?:" + computed_constraints + ")"+(this.required?"":"?")+"$")).test(value) &&
                    (!this.required || !this._isEmpty(value)) &&
                    (this._isEmpty(value) || this.parse(value, constraints) !== undefined); // Boolean
            }
        });
    }
);

/* TabContainer EXTENDED Version */
require(["dojo/_base/lang", "dojo/_base/array", "dijit/layout/TabContainer"],
    function(lang, array, TabContainer) {
        lang.extend(TabContainer, {
            hasChild: function (tabId) {
                return array.some(this.getChildren(), function(c) {
                    return Boolean(c.id == tabId);
                });
            }
        });
    }
);

/* Menu EXTENDED Version */
require(["dojo/_base/lang", "dojo/_base/array", "dijit/Menu"],
    function(lang, array, Menu) {
        lang.extend(Menu, {
            removeChildren: function () {
                var _this = this;
                array.map(this.getChildren(), function(c) {
                    _this.removeChild(c);
                });
            }
        });
    }
);
