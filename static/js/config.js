/* avoid hardcoded values all over javascript files */

define({
    urls: {
        'md-get-part': '/md/get-part/',
        'md-get-form': '/md/get-form/',

        /* account.js */
        'md-get-qr-code': '/md/get-qr-code/',
        'md-get-totp-current-value': '/md/get-totp-current-value/',
        'md-set-otp-key': '/md/set-otp-key/',
        'md-cancel-otp-check': '/md/cancel-otp-check/',

        /* lists.js */


        /* templates.js */


        /* campaigns.js */
    },

    somethingelse: {

    }
});