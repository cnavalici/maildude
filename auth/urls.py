from django.conf.urls import patterns, url
from django.http import HttpResponseRedirect

urlpatterns = patterns('',
    url(r'^login/$', 'auth.views.login', name='login'),
    url(r'^logout/$', 'auth.views.logout', name='logout'),
    url(r'^success/$', lambda x: HttpResponseRedirect('/md/'), name='success'),

    url(r'^password/reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        {'template_name': 'auth/password_reset_confirm.html',
        'post_reset_redirect' : '/auth/password/reset/complete/'},
        name='password_reset_confirm'),

    url(r'^password/reset/complete/$',
        'django.contrib.auth.views.password_reset_complete',
        {'template_name': 'auth/password_reset_complete.html'},
        name='password_reset_complete'),

    url(r'^$', lambda x: HttpResponseRedirect('login/')),
)
