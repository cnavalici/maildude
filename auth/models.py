from django.db import models
from django.contrib.auth.models import User


class AuthUser(User):
    otp_token = models.CharField(max_length=16, blank=True, default='', null=True)
    is_first_login = models.BooleanField(default=True)

    def __unicode__(self):
        return u"{0} {1} ({2})".format(self.first_name, self.last_name, self.username)
