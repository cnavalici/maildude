from django.shortcuts import render_to_response
from django.template import RequestContext

from django.contrib.auth import logout as django_logout
from django.contrib.auth import login as django_login
from django.contrib.auth.views import login as django_login_view
from django.contrib.auth import authenticate

from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from auth.forms import AuthTokenLoginForm


def login(request):
    if request.method == 'POST':
        login_form = AuthTokenLoginForm(data=request.POST)

        if login_form.is_valid():
            username = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']
            otp_token = login_form.cleaned_data['otp_token']

            authenticated_user = authenticate(username=username, password=password, otp_token=otp_token)

            if authenticated_user:
                django_login(request, authenticated_user)

                #from django.db import models
                #models.Manager.client_id = authenticated_user.client_id

                return HttpResponseRedirect(reverse('auth:success'))

    return django_login_view(request, template_name='auth/login.html', authentication_form=AuthTokenLoginForm)


def logout(request):
    django_logout(request)
    return HttpResponseRedirect(reverse('auth:login'))


def success(request):
    return render_to_response('auth/success.html', {}, context_instance=RequestContext(request))
