from django.conf import settings

from utils.helpers import get_menu_items


def global_settings(request):
    ''' prefix every variable with GL_ '''
    return {
        'GL_VERSION': settings.VERSION,
        'GL_APP_NAME': settings.APP_NAME,
    }


def processor(request):
    full_path = request.get_full_path()

    context = {
        # Don't show messages on AJAX requests (they are deleted if shown)
        'no_messages': request.META.get('HTTP_X_WGER_NO_MESSAGES', False),
    }

    if request.user.is_authenticated():
        context['menu_items'] = get_menu_items()

        # Pseudo-intelligent navigation here
        if '/my-account/' in request.get_full_path():
            context['active_tab'] = settings.MN_ACCOUNT

        elif '/campaigns/' in request.get_full_path():
            context['active_tab'] = settings.MN_CAMPAIGNS

        elif '/lists/' in request.get_full_path():
            context['active_tab'] = settings.MN_LISTS

        elif '/templates/' in request.get_full_path():
            context['active_tab'] = settings.MN_TEMPLATES

        else:
            context['active_tab'] = settings.MN_DASHBOARD

    return context
