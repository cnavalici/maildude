# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.conf import settings

import os
import os.path

# from pyotp import random_base32, TOTP


def get_menu_items():
    '''used to generate the navigation menu'''
    menu_items = [
        # url, name
        {'url': reverse('md:dashboard'), 'name': _('Dashboard'), 'code': settings.MN_DASHBOARD},
        {'url': reverse('md:campaigns_page'), 'name': _('Campaigns'), 'code': settings.MN_CAMPAIGNS},
        {'url': reverse('md:lists_page'), 'name': _('Lists'), 'code': settings.MN_LISTS},
        {'url': reverse('md:templates_page'), 'name': _('Templates'), 'code': settings.MN_TEMPLATES},
        {'url': reverse('md:account_page'), 'name': _('Reports'), 'code': settings.MN_REPORTS},
        {'url': reverse('md:account_page'), 'name': _('MyAccount'), 'code': settings.MN_ACCOUNT},
    ]

    return menu_items
