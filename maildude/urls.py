from django.conf.urls import patterns, include, url
from django.http import HttpResponseRedirect

urlpatterns = patterns('',
    url(r'^$', lambda x: HttpResponseRedirect('auth/')),
    url(r'^auth/', include('auth.urls', namespace="auth")),
    url(r'^md/', include('md.urls', namespace="md")),
)
